# Prerequisites per role

## shell (optional)

If needed, the file .shell_extra.skel can be copied and modified to set environment variables.

# Execution command:

```
ansible-playbook --diff -c local [--tags] --extra-vars "@~/.config/eitr/values.yaml" -K <playbook>
```

Where --tags is optional in case only some roles should be installed and <playbook> is the playbook
we want to use

# Configure the playbook

1. Create the file `~/.config/eitr/values.yaml`
2. Fill the *required* variables or face impending doom
3. Let the eitr loose and watch it create life as we know it (run the playbook)

## Key-value pairs explained

|--------------------|--------|------------------|-------|----------|
| Variable           | Type   | Example          | Role  | Required |
|--------------------|--------|------------------|-------|----------|
| mpd_music_dir      | string | ~/music/         | mpd   | yes      |
| filebot_input_dir  | string | ~/filebot/input  | shell | no       |
| filebot_output_dir | string | ~/filebot/output | shell | no       |
| autobackup_to      | string | dir1:dir2        | shell | no       |
| autobackup_from    | string | dir1:dir2        | shell | no       |
| git_user_email     | string | username@xyz.com | git   | yes      |
| git_user_name      | string | username         | git   | yes      |
| wallpaper_src      | string | ~/wallpapers     | shell | no       |
|--------------------|--------|------------------|-------|----------|
