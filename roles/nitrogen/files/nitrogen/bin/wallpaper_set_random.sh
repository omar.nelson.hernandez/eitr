#!/usr/bin/env sh

PAPES_SRC=${WALLPAPER_SRC:-No wallpaper directory set}

for i in $(seq 1 $(xrandr | grep -v disconnected | grep -c connected));
do
  nitrogen --set-zoom-fill --random --head=$(( ${i} - 1 )) ${PAPES_SRC}
done
