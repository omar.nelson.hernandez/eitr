#!/usr/bin/env bash

WORK_DIR=${1:?No working directory specified}
START_TIME=$(date +'%s')

mkdir -p ${WORK_DIR}/added-magnets

IFS=$'\n'

for FILENAME in $(ls ${WORK_DIR}/*.magnet)
do
  FILE=$(basename ${FILENAME})
  echo "FILENAME >>>> $FILENAME"
  echo "FILE >>>> $FILE"
  TORRENT=$(cat ${FILENAME})
  echo "TORRENT >>>> $TORRENT"
  RET_STR=$(transmission-remote localhost:9091 --add ${TORRENT})
  if [ $? -eq 0 ]; then
    echo "Success with: ${FILENAME}"
    mv ${FILENAME} ${WORK_DIR}/added-magnets/${FILE}.added.${START_TIME}
  else
    echo "Failed ${FILENAME} with: ${RET_STR}"
  fi
done

unset IFS
