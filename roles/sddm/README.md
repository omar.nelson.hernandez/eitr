SDDM may need some configuration hints for the displays, when looking at journalctl, these seem to
be the workflow:

```
May 21 09:17:23 debian systemd[1]: Started Simple Desktop Display Manager.
May 21 09:17:23 debian sddm[936]: Initializing...
May 21 09:17:23 debian sddm[936]: Starting...
May 21 09:17:23 debian sddm[936]: Logind interface found
May 21 09:17:23 debian sddm[936]: Adding new display on vt 7 ...
May 21 09:17:23 debian sddm[936]: Loading theme configuration from ""
May 21 09:17:23 debian sddm[936]: Display server starting...
May 21 09:17:23 debian sddm[936]: Adding cookie to "/var/run/sddm/{6ac948c2-d4fd-4985-8740-3e539c7ad318}"
May 21 09:17:23 debian sddm[936]: Running: /usr/bin/X -nolisten tcp -auth /var/run/sddm/{6ac948c2-d4fd-4985-8740-3e539c7ad318} -background none -noreset -displayfd 17 -seat seat0 vt7
May 21 09:17:25 debian sddm[936]: Setting default cursor
May 21 09:17:25 debian sddm[936]: Running display setup script  "/usr/share/sddm/scripts/Xsetup"
May 21 09:17:25 debian sddm[936]: Display server started.
May 21 09:17:25 debian sddm[936]: Socket server starting...
May 21 09:17:25 debian sddm[936]: Socket server started.
May 21 09:17:25 debian sddm[936]: Loading theme configuration from "/usr/share/sddm/themes/debian-theme/theme.conf"
May 21 09:17:25 debian sddm[936]: Greeter starting...
May 21 09:17:25 debian sddm-helper[1340]: [PAM] Starting...
May 21 09:17:25 debian sddm-helper[1340]: [PAM] Authenticating...
May 21 09:17:25 debian sddm-helper[1340]: [PAM] returning.
May 21 09:17:25 debian sddm-helper[1340]: pam_unix(sddm-greeter:session): session opened for user sddm(uid=114) by (uid=0)
May 21 09:17:25 debian sddm[936]: Greeter session started successfully
May 21 09:17:26 debian sddm[936]: Message received from greeter: Connect
May 21 09:19:31 debian sddm[936]: Message received from greeter: Login
May 21 09:19:31 debian sddm[936]: Reading from "/usr/share/xsessions/i3.desktop"
May 21 09:19:31 debian sddm[936]: Reading from "/usr/share/xsessions/i3.desktop"
May 21 09:19:31 debian sddm[936]: Session "/usr/share/xsessions/i3.desktop" selected, command: "i3"
May 21 09:19:31 debian sddm-helper[1396]: [PAM] Starting...
May 21 09:19:31 debian sddm-helper[1396]: [PAM] Authenticating...
May 21 09:19:31 debian sddm-helper[1396]: [PAM] Preparing to converse...
May 21 09:19:31 debian sddm-helper[1396]: [PAM] Conversation with 1 messages
May 21 09:19:31 debian sddm-helper[1396]: gkr-pam: unable to locate daemon control file
May 21 09:19:31 debian sddm-helper[1396]: gkr-pam: stashed password to try later in open session
May 21 09:19:31 debian sddm-helper[1396]: [PAM] returning.
May 21 09:19:31 debian sddm[936]: Authenticated successfully
May 21 09:19:31 debian sddm-helper[1396]: pam_unix(sddm:session): session opened for user rurushu(uid=1000) by (uid=0)
May 21 09:19:31 debian sddm[936]: Auth: sddm-helper exited successfully
May 21 09:19:31 debian sddm[936]: Greeter stopped.
May 21 09:19:32 debian sddm-helper[1396]: gkr-pam: gnome-keyring-daemon started properly and unlocked keyring
May 21 09:19:32 debian sddm-helper[1396]: Starting: "/etc/sddm/Xsession \"i3\""
May 21 09:19:32 debian sddm[936]: Session started
```

The takeaway from this is that `/usr/share/sddm/scripts/Xsetup` is the right place to try to
configure the displays and adding the following line should work:

```
autorandr --load desktop
```
