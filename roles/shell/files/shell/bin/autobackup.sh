#!/usr/bin/env bash

. ~/.shell_functions

RESTORE=0

while :; do
  case $1 in
    --restore)
      RESTORE=1
      ;;
    *)
      break
  esac
  shift
done

IFS=':'

if [[ ${RESTORE} -eq 0 ]]; then
  read -r -a FROM_ARR <<< ${BKP_FROM:?Variable not found}
  read -r -a TO_ARR <<< ${BKP_TO:?Variable not found}
else
  read -r -a FROM_ARR <<< ${BKP_REST_FROM:?Variable not found}
  read -r -a TO_ARR <<< ${BKP_REST_TO:?Variable not found}
fi

if [[ ${#FROM_ARR[@]} -ne ${#TO_ARR[@]} ]]; then
  echo "Sizes of BKP_FROM (${#FROM_ARR[@]}) and BKP_TO (${#TO_ARR[@]}) are different"
  exit 1
fi

for i in ${!FROM_ARR[@]};
do
  echo "Syncing from <${FROM_ARR[i]}> to <${TO_ARR[i]}>"
done

echo
read -p "Please confirm to engage in thermonuclear warfare (Y/N) " -n 1 -r
echo
if [[ ! ${REPLY} =~ ^[Yy]$ ]]; then
  echo "Weapon systems disengaged"
  exit 0
fi

echo "Engaging missiles"

for i in ${!FROM_ARR[@]};
do
  rs ${FROM_ARR[i]} ${TO_ARR[i]} &
done

unset IFS
